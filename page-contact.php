<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 * 
 * Template name: Template contatti
 */

$map = get_field('map');
$address = get_field('address');
$form = get_field('form');
get_header(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('c-page'); ?>>
		<section class="c-contact__header">
			<?php the_title('<h1 class="c-page__title">','</h1>') ?>	
			<div id="map"></div>
			<!-- <div style="overflow:hidden;width: 1920px;position: relative;"><iframe width="1920" height="600" src="https://maps.google.com/maps?width=1920&amp;height=600&amp;hl=en&amp;q=VIA%20COMMERCIALE%2C%20160%2F2%2C%2034134%2C%20Trieste(TS)+(Titolo)&amp;ie=UTF8&amp;t=&amp;z=17&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="https://embedgooglemaps.com/en/">embedgooglemaps EN</a> & <a href="http://linkmatch.info">link Match</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><br /> -->
			</div>
		</section>

	<div class="c-contact l-container">
		<div class="c-contact__info">
			<?php if ($address['title']): ?>
				<h2 class="title"><?php echo $address['title'] ?></h2>
			<?php endif ?>
			<?php if ($address['text']): ?>
				<?php echo $address['text'] ?>
			<?php endif ?>

			<div class="c-contact__social">
			<?php if ($address['facebook']): ?>
				<a href="<?php echo $address['facebook'] ?>" target="_blank" class="c-social c-social__facebook"></a>
			<?php endif ?>
			<?php if ($address['youtube']): ?>
				<a href="<?php echo $address['youtube'] ?>" target="_blank" class="c-social c-social__youtube"></a>
			<?php endif ?>
			</div>
		</div>

		<div class="c-contact__form">
		<?php if ($form['title']): ?>
			<h2><?php echo $form['title'] ?></h2>
		<?php endif ?>
		<?php if ($form['title']): ?>
			<?php echo $form['shortcode'] ?>
		<?php endif ?>
		</div>
	</div>
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'tennis_tavolo_ts' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php get_footer(); ?>

<script>
// Initialize and add the map
function initMap() {
	var position = {lat: <?php echo $map['lat'] ?>, lng: <?php echo $map['lng'] ?>};
	var map = new google.maps.Map(
		document.getElementById('map'), {
			zoom: 16, 
			center: position,
			disableDefaultUI: true,
			});
	var marker = new google.maps.Marker({
		position: position, 
		map: map,
		icon:'<?php echo $map['pin'] ?>',
		});

	// Create a new StyledMapType object, passing it an array of styles,
	// and the name to be displayed on the map type control.
	var styledMapType = new google.maps.StyledMapType(
		[
			{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#6195a0"
					}
				]
			},
			{
				"featureType": "administrative.province",
				"elementType": "geometry.stroke",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "geometry",
				"stylers": [
					{
						"lightness": "0"
					},
					{
						"saturation": "0"
					},
					{
						"color": "#f5f5f2"
					},
					{
						"gamma": "1"
					}
				]
			},
			{
				"featureType": "landscape.man_made",
				"elementType": "all",
				"stylers": [
					{
						"lightness": "-3"
					},
					{
						"gamma": "1.00"
					}
				]
			},
			{
				"featureType": "landscape.natural.terrain",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#bae5ce"
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 45
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#fac9a9"
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "labels.text",
				"stylers": [
					{
						"color": "#4e4e4e"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#787878"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "transit.station.airport",
				"elementType": "labels.icon",
				"stylers": [
					{
						"hue": "#0a00ff"
					},
					{
						"saturation": "-77"
					},
					{
						"gamma": "0.57"
					},
					{
						"lightness": "0"
					}
				]
			},
			{
				"featureType": "transit.station.rail",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#43321e"
					}
				]
			},
			{
				"featureType": "transit.station.rail",
				"elementType": "labels.icon",
				"stylers": [
					{
						"hue": "#ff6c00"
					},
					{
						"lightness": "4"
					},
					{
						"gamma": "0.75"
					},
					{
						"saturation": "-68"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
					{
						"color": "#eaf6f8"
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#c7eced"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"lightness": "-49"
					},
					{
						"saturation": "-53"
					},
					{
						"gamma": "0.79"
					}
				]
			}
		],
		{name: 'Styled Map'});


	//Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('styled_map', styledMapType);
	map.setMapTypeId('styled_map');
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3JC1NZgzElfx7VNXOWpZJ0BsJOTGaU0&callback=initMap">
</script>
