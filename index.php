<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 */

get_header();

if (is_home() && get_option('page_for_posts') ) {
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'full'); 
    $featured_image = $img[0];
}

?>

<section <?php post_class('c-page'); ?>>
		<section class="c-page__header">
			<h1 class="c-page__title">
				<?php single_post_title() ?>
			</h1>
			<div class="c-page__thumbnail">
				<div style="background-image:url('<?php echo $featured_image ?>')"></div>
			</div>
			<select class="o-select c-news__archive" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
				<option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
				<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
			</select>
		</section>
	<div class="c-news l-container">
	<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'news' );
			endwhile;
			?><div class="c-news__paginator"><?php
			the_posts_navigation();
			?></div><?php
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;
		?>	
	</div>

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'tennis_tavolo_ts' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</section>

<?php get_footer();
