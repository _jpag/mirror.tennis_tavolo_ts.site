<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 * 
 * Template name: Template corsi
 */
$link = get_field('link');	
$image = get_field('image');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
$alt = array('alt'=>get_the_title());
$image_bg = wp_get_attachment_image_src($image, $size);

$category_detail=get_the_category($post->ID);
foreach($category_detail as $cd){
	$category_name = $cd->name;
	$category_slug = $cd->slug;
}

get_header();
get_sidebar('courses');

?>
<div class="c-courses">
	<?php while ( have_posts() ) :	?>

	<div class="c-courses__content">
		<?php 
			the_title('<h1>','</h1>');
			the_post(); 
			the_content(); 
		?>
	</div>
	<?php endwhile; ?> 

</div>
<?php edit_post_link(
	sprintf(
		wp_kses(
			/* translators: %s: Name of current post. Only visible to screen readers */
			__( 'Edit <span class="screen-reader-text">%s</span>', 'tennis_tavolo_ts' ),
			array(
				'span' => array(
					'class' => array(),
				),
			)
		),
		get_the_title()
	),
	'<span class="edit-link">',
	'</span>');
?>
<?php get_footer(); ?>
