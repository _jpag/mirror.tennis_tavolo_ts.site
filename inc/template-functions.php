<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package tennis_tavolo_ts
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function tennis_tavolo_ts_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'tennis_tavolo_ts_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function tennis_tavolo_ts_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'tennis_tavolo_ts_pingback_header' );


/**
 * ACF Gutenberg Blocks
 */
add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
	if( function_exists('acf_register_block') ) {	
		// register a slideshow block
		acf_register_block(array(
			'name'				=> 'slideshow',
			'title'				=> __('Slideshow'),
			'description'		=> __('A custom slideshow block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'images-alt2',
			'keywords'			=> array( 'slideshow', 'carosello' ),
		));
		// register a last news block
		acf_register_block(array(
			'name'				=> 'last-news',
			'title'				=> __('News in evidenza'),
			'description'		=> __('A custom last news block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-post',
			'keywords'			=> array( 'last_news', 'news' ),
		));
		// register a full width teaser
		acf_register_block(array(
			'name'				=> 'teaser-full',
			'title'				=> __('Fascia teaser full width'),
			'description'		=> __('A custom full width teaser.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'welcome-widgets-menus',
			'keywords'			=> array( 'teaser', 'teaser' ),
		));
		// register a 2 half width teaser
		acf_register_block(array(
			'name'				=> 'teasers-half',
			'title'				=> __('Fascia con 2 teaser'),
			'description'		=> __('Two half width teaser.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'welcome-widgets-menus',
			'keywords'			=> array( 'teaser', 'teaser' ),
		));
		// register a row block
		acf_register_block(array(
			'name'				=> 'row',
			'title'				=> __('Fascia con immagine e testo'),
			'description'		=> __('A custom row block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'align-left',
			'keywords'			=> array( 'row', 'riga' ),
		));
		// register a intro page block
		acf_register_block(array(
			'name'				=> 'intro',
			'title'				=> __('Fascia per introduzione di pagina'),
			'description'		=> __('A custom intro page block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'align-right',
			'keywords'			=> array( 'intro', 'introduzione' ),
		));
		// register a open day block
		acf_register_block(array(
			'name'				=> 'open-day',
			'title'				=> __('Orari settimanali'),
			'description'		=> __('A custom open day block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'clock',
			'keywords'			=> array( 'day', 'orari settimanale' ),
		));
		acf_update_setting('google_api_key', 'AIzaSyBrCDdwb9CpTMKo50WxZBTpr49i9LEa-gY');
	}
}
function my_acf_block_render_callback( $block ) {	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") );
	}
}

/**
 * Hide editor in template
 */
add_action('init', 'my_rem_editor_from_post_type');
function my_rem_editor_from_post_type() {
    remove_post_type_support( 'player', 'editor' );
}

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);