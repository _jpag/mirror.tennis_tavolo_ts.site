<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 */

?>


<a id="post-<?php the_ID(); ?>" href="<?php echo get_permalink($post_id) ?>" class="c-lastnews__item withMarginBottom">
	<div>
		<p class="c-lastnews__date"><?php echo get_the_date() ?></span></p>
		<h3 class="c-lastnews__title"><?php echo get_the_title() ?></h3>
	</div>
</a>
