<?php
	/**
	 * Block Name: Teaser full width
	 */
	$title = get_field('title');
	$text = get_field('text');
	$image = get_field('image');
	$link1 = get_field('link_1');	
	$link2 = get_field('link_2');	
	$size = 'large'; // (thumbnail, medium, large, full or custom size)	
	$alt = array('alt'=>$title);
?>
<div class="c-intro l-container">
	<div class="c-intro__content">
		<?php if ($title): ?>
			<h1><?php echo $title ?></h1>
		<?php endif ?>
		<?php if ($text): ?>
			<?php echo $text ?>
		<?php endif ?>
		<?php if ($link1): ?>
			<a class="o-button js-anchorScroll" target="<?php echo $link1['target'] ?>" href="<?php echo $link1['url'] ?>"><?php echo $link1['title'] ?></a>
		<?php endif ?>
		<?php if ($link2): ?>
			<a class="o-button o-button_yellow js-anchorScroll" target="<?php echo $link2['target'] ?>" href="<?php echo $link2['url'] ?>"><?php echo $link2['title'] ?></a>
		<?php endif ?>
	</div>
	<div class="c-intro__image">
		<div class="u-cover-image">
			<?php if( $image ) {
				echo wp_get_attachment_image($image, $size, false, $alt);
			} ?>
		</div>
	</div>
</div>

