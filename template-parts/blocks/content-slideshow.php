<?php
	/**
	 * Block Name: Showreel
	 */
	$count = 0;
?>
<?php if(get_field('slideshow')): ?>
	<section class="c-slideshow">
		<?php while(has_sub_field('slideshow')): ?>
			<div class="c-slideshow__slide item">
				<?php 
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$link = get_sub_field('link');
					$size = 'full'; // (thumbnail, medium, large, full or custom size)
					$alt = array('alt'=>$title);
					$image_bg = wp_get_attachment_image_src($image, $size);
				?>
				<div class="c-slideshow__image">
					<div class="u-cover-image">
						<?php if( $image ) {
							echo wp_get_attachment_image($image, $size, false, $alt);
						} ?>
					</div>
				</div>

				<div class="c-slideshow__content">
					<h4>ASD TTTS</h4>
					<h2><?php echo $title ?></h2>
					<?php if ($link): ?>
					<a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
					<?php endif ?>
				</div>
				<img class="c-img__racket js-move02" src="<?php echo get_template_directory_uri().'/images/img_racchetta.png' ?>" alt="Racchetta Pin Pong Trieste Sistiana">

				<div class="c-slideshow__bg"><div style="background-image: url(<?php echo $image_bg[0] ?>)"></div></div>				
			</div>
			<?php $count++ ?>
		<?php endwhile; ?>
		
		<img class="c-img__ball js-move01" src="<?php echo get_template_directory_uri().'/images/img_pallina.png' ?>" alt="Pallina Ping Pong Trieste Sistiana">
		<?php if ($count > 1): ?>
			<div class="c-slideshow__pager"></div>
		<?php endif ?>

	</section>
<?php endif; ?>

