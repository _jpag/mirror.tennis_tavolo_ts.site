<?php
	/**
	 * Block Name: Teaser full width
	 */
	$title = get_field('title');
	$image = get_field('image');
	$link = get_field('link');	
?>
<section class="c-teaser__full" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		<?php if ($title): ?>
			<h2 class="title"><?php echo $title ?></h2>
		<?php endif ?>
		<?php if ($link): ?>
			<a class="o-button o-button_light" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
		<?php endif ?>
	</div>
</section>

