<?php
	/**
	 * Block Name: Teaser full width
	 */
	$title1 = get_field('title_1');
	$text1 = get_field('text_1');
	$image1 = get_field('image_1');
	$link1 = get_field('link_1');	

	$title2 = get_field('title_2');
	$text2 = get_field('text_2');
	$image2 = get_field('image_2');
	$link2 = get_field('link_2');	

	$layout = 'full';
?>

<?php if ($title2):
	$layout = 'half';
endif ?>

<section class="c-teasers">
	<div class="c-teasers__item <?php echo $layout ?>" style="background-image: url('<?php echo $image1; ?>')">
		<?php if ($title1): ?>
			<h2><?php echo $title1 ?></h2>
			<p><?php echo $text1 ?></p>
		<?php endif ?>
		<?php if ($link1): ?>
			<a class="o-button o-button_light" target="<?php echo $link1['target'] ?>" href="<?php echo $link1['url'] ?>"><?php echo $link1['title'] ?></a>
		<?php endif ?>
	</div>

	<?php if ($title2): ?>
		<div class="c-teasers__item <?php echo $layout ?>" style="background-image: url('<?php echo $image2; ?>')">
			<h2><?php echo $title2 ?></h2>
			<p><?php echo $text2 ?></p>
			<?php if ($link2): ?>
			<a class="o-button o-button_light" target="<?php echo $link2['target'] ?>" href="<?php echo $link2['url'] ?>"><?php echo $link2['title'] ?></a>
			<?php endif ?>
		</div>
	<?php endif ?>
</section>
