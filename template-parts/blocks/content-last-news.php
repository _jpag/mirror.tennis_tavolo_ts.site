
<?php
	/**
	 * Block Name: News in evidenza
	 */
?>
<section class="c-lastnews l-container">
	<div class="c-lastnews__header">
		<h2 class="title">Ultime news</h2>
		<a class="o-button o-button_outline" href="<?php echo home_url() ?>/news">tutte le news</a>
	</div>
	<div class="c-lastnews__inner">
		<?php
			$args = array( 'numberposts' => '3' );
			$recent_posts = wp_get_recent_posts( $args );
			foreach( $recent_posts as $recent ){
				$image = get_the_post_thumbnail($recent["ID"], medium);
				echo 
					'<a href="' . get_permalink($recent["ID"]) . '" class="c-lastnews__item">
					'.(($image)? '<div class="image">'. $image .'</div>':'').'
						<div>
							<p class="c-lastnews__date">' . date('j F Y', strtotime($recent['post_date'])) . '</span></p>
							<h3 class="c-lastnews__title">' . $recent["post_title"] . '</h3>
							<p>' . $recent["post_excerpt"] . '</p>
						</div>
					</a>';
			}
			wp_reset_query();
		?>
		</div>
	</section>