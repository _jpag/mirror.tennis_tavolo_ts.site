<?php
	/**
	 * Block Name: Teaser full width
	 */
	$title = get_field('title');
	$text = get_field('text');
	$image = get_field('image');
	$link = get_field('link');	
	$style = get_field('style');
	$size = 'large'; // (thumbnail, medium, large, full or custom size)	
	$alt = array('alt'=>$title);
	$ID = get_field('id');
?>
<div id="<?php echo $ID ?>" class="c-pagerow c-pagerow--<?php echo $style ?> l-container">
	<div class="c-pagerow__content">
		<?php if ($title): ?>
			<h2><?php echo $title ?></h2>
		<?php endif ?>
		<?php if ($text): ?>
			<?php echo $text ?>
		<?php endif ?>
		<?php if ($link): ?>
			<a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
		<?php endif ?>
	</div>

	<div class="c-pagerow__image">
	<div class="u-cover-image">
	<?php if( $image ) {
		echo wp_get_attachment_image($image, $size, false, $alt);
	} ?>
	</div>
	</div>
</div>

