<?php
	/**
	 * Block Name: Teaser full width
	 */
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );

?>

<?php if ($title2):
	$layout = 'half';
endif ?>

<?php if ($image) : ?>
	<section class="c-page__header">
		<?php the_title('<h1 class="c-page__title">','</h1>') ?>
		<div class="c-page__thumbnail">
			<div style="background-image: url(<?php echo $image[0] ?>)"></div>
		</div>
	</section>
<?php endif ?>

