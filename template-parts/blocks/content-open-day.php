<?php
	/**
	 * Block Name: Ooen day block
	 */
	$title = get_field('title');

?>
<div class="c-openDay">
	<?php if ($title): ?>
		<h2><?php echo $title ?></h2>
	<?php endif ?>
	
	<?php while(has_sub_field('open_day')): ?>
		<?php 
			$day = get_sub_field('day');
			$hour = get_sub_field('hour');
		?>
	<div class="c-openDay__table">
		<p><strong><?php echo $day ?>: </strong><?php echo $hour ?></p>	
	</div>
	<?php endwhile; ?>
</div>


