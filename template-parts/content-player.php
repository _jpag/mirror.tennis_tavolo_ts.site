<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 */

?>

<div>
	<?php the_content() ?>
</div>
