<?php
/**
 * Template part for displaying page content in page-simple.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 */
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('c-page'); ?>>
	<?php if ($image) : ?>
		<section class="c-page__header">
			<?php the_title('<h1 class="c-page__title">','</h1>') ?>
			<div class="c-page__thumbnail">
				<div style="background-image: url(<?php echo $image[0] ?>)"></div>
			</div>
		</section>
	<?php endif ?>

	<div class="entry-content c-page__body">
			<?php the_content(); ?>
		</div>
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'tennis_tavolo_ts' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
