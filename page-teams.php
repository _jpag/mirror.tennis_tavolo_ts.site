<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 * 
 * Template name: Template squadre
 */
$link = get_field('link');	
$image = get_field('image');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
$alt = array('alt'=>get_the_title());
$image_bg = wp_get_attachment_image_src($image, $size);

$category_detail=get_the_category($post->ID);
foreach($category_detail as $cd){
	$category_name = $cd->name;
	$category_slug = $cd->slug;
}

get_header();
get_sidebar();

?>
<div class="c-teams c-page">
	<?php while ( have_posts() ) :	?>
	<div class="c-teams__header">
		<div class="c-teams__image">
			<div class="u-cover-image">
				<?php if( $image ) {
					echo wp_get_attachment_image($image, $size, false, $alt);
				} ?>
			</div>
		</div>
		<div class="c-teams__title">
			<?php the_title( '<h1 class="title">', '</h1>' ); ?>
			<h4><?php echo $category_name ?></h4>
		</div>
		<?php if($link): ?>
			<a class="c-teams__btn o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
		<?php endif ?>
	
		<div class="c-teams__bg"><div style="background-image: url(<?php echo $image_bg[0] ?>)"></div></div>				
	</div>

	<div class="c-teams__content">
		<?php 
			the_post(); 
			the_content(); 
		?>
	</div>
	<?php endwhile; ?> 
	<div class="c-teams__team">
		<?php
		$query = new WP_Query( array('posts_per_page' => 99, 'category_name' => $category_slug, 'post_type' => 'player' ) );
		if ( $query->have_posts() ):
			while ( $query->have_posts() ) : $query->the_post();		
				$name = get_field('name');
				$year = get_field('year');
				$image = get_field('image');
				$lastname = get_field('lastname');
				$rank = get_field('ranking');
				?>	
				<div class="c-player">
					<h2><?php echo $name ?> <?php echo $lastname ?></h2>
					<p>Anno di nascita: <?php echo $year ?></p>
					<br>
					<?php if($image): ?>
						<div class="c-player__image" style="background-image: url(<?php echo $image ?>)"></div>
					<?php endif ?>
					<span class="c-player__ranking">RANKING <strong><?php echo $rank ?></strong></span>
				</div>
				<?php
			endwhile;
		endif;
	?>
	</div>
</div>
<?php edit_post_link(
	sprintf(
		wp_kses(
			/* translators: %s: Name of current post. Only visible to screen readers */
			__( 'Edit <span class="screen-reader-text">%s</span>', 'tennis_tavolo_ts' ),
			array(
				'span' => array(
					'class' => array(),
				),
			)
		),
		get_the_title()
	),
	'<span class="edit-link">',
	'</span>');
?>
<?php get_footer(); ?>
