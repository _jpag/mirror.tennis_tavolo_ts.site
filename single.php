<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tennis_tavolo_ts
 */
$attachment = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );

if ($attachment) {
	$image = $attachment[0];
} else {
	$image = get_template_directory_uri() . '/images/tennis-tavolo-trieste-sistiana-01.jpg';
}
$headerClass = $attachment ? 'withImage' : '' ;
get_header();
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('c-page'); ?>>
		<?php if ($image) : ?>
			<section class="c-page__header <?php echo $headerClass ?>">
				<?php if ($attachment) : ?>
				<div class="c-page__image">
					<div class="u-cover-image">
					<img src="<?php echo $attachment[0] ?>" alt="<?php echo the_title() ?>">
					</div>
				</div>
				<?php endif ?>
				<?php the_title('<h1 class="c-page__title c-news__title">','</h1>') ?>
				<div class="c-page__thumbnail">
					<div style="background-image: url(<?php echo $image ?>)"></div>
				</div>
			</section>
		<?php endif ?>
		<?php
		while ( have_posts() ) :
			the_post();
			?><div class="c-news__body">
				<h4 class="c-news__date"><?php the_date(); ?></h4>
			<?php the_content(); ?>

		</div><?php
			// the_post_navigation();

			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		endwhile; 
		?>
	</article><!-- #primary -->

<?php
get_footer();