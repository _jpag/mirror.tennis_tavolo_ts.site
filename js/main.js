(function ($) {
    var showreelInitializer = function () {
        var sl = $(".c-slideshow");
        if (sl.length > 0) {
            sl.cycle({
                timeout: 5000,
                speed: 800,
                next: '.next',
                prev: '.prev',
                slides: '.item',
                swipe: true,
                fx: 'fadeout',
                log: false,
                //swipeFx: 'scrollHorz',
                pager: '.c-slideshow__pager',
                pagerTemplate: '<a href="javascript:;"></a>',
                pagerActiveClass: 'isActive'
            });
        }
    };

    var togglerMenuMobile = function() {
        $(document).on("click", ".js-navMobile", function () {
            $('body').toggleClass("openMenu");
        });
    };

    var menuMobile = function(e) {
        var el = $('.js-noPage').children('a');
        var w = $(window).innerWidth();
        if (el.length > 0 && w < 1024) {
           el.on('click', function(e) {
                e.preventDefault();
                el.siblings('.sub-menu').removeClass('isOpen');
                $(this).siblings('.sub-menu').addClass('isOpen');
           });
        }
    };

    var anchorScroll = function() {
      $('.js-anchorScroll').click(function(){
            $('html, body').animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 120
            }, 500);
            return false;
        });
    };

    var teamToggler = function() {
      var btn = $('.js-teamToggler');
      btn.on('click', function() {
        $(this).siblings('.c-sidebar').toggleClass('isOpen');
      });
    };

    $(document).ready(function () {
        showreelInitializer();
        togglerMenuMobile();
        menuMobile();
        anchorScroll();
        teamToggler();
    })
    
    //--------------------
    //MOUSE MOVE FX - global variables
    //--------------------
    var elOne = $('.js-move01');
    var elTwo = $('.js-move02');
    
    //--------------------
    //global functions
    //--------------------
    function moveMask(event, el, operX, operY) {
      var winX = $(window).innerWidth()/2;
      var winY = $(window).innerHeight()/2;
      var posX = ((event.pageX - winX)/ operX).toFixed(0);
      var posY = ((event.pageY - winY)/ operY).toFixed(0);
      el.css({
        transform: 'translate('+posX+'px, '+posY+'px)',
        '-webkit-transform': 'translate('+posX+'px, '+posY+'px)',
        '-moz-transform': 'translate('+posX+'px, '+posY+'px)',
        '-ms-transform': 'translate('+posX+'px, '+posY+'px)',
      });
    };
    function resetPos(el) {
      el.css({
        transform: 'translate(0, 0)',
        '-webkit-transform': 'translate(0, 0)',
        '-moz-transform': 'translate(0, 0)',
        '-ms-transform': 'translate(0, 0)',
      });
    };
    
    
    //--------------------
    //launch functions
    //--------------------
    $(document,window,'html').on('mousemove', function(event){
      setTimeout(
        moveMask(event, elOne, 30, 30)
        ,1600);
    }).mouseleave(function() {
      resetPos(elOne);
    });
    
    $(document,window,'html').on('mousemove', function(event){
      setTimeout(
        moveMask(event, elTwo, -60, -40)
        ,2800);
    }).mouseleave(function() {
      resetPos(elTwo);
    });
})(jQuery);
