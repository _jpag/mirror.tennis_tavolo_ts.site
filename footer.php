<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tennis_tavolo_ts
 */

?>

	</div><!-- #content -->
</div><!-- #page -->

<footer id="colophon" class="c-footer">
	<div class="inner l-container">
		<div class="c-footer__name">
			<?php if (!dynamic_sidebar('footername') ) : endif; ?>
		</div>
		<div class="c-footer__info">
			<?php if (!dynamic_sidebar('footerinfo') ) : endif; ?>
		</div>
	</div><!-- .site-info -->
	<div class="c-footer__powered">
		<a href="http://www.brainupstudio.it/" target="_blank"> 
		<p>POWERED BY</p>
		<img src="<?php echo get_template_directory_uri() ?>/images/brainup-logo-h.svg" alt="BrainUp Web design" title="BrainUp Studio - Web design Trieste">
		</a>
	</div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
