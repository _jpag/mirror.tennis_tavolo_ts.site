<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tennis_tavolo_ts
 * 
 * Template name: Template pagina semplice
 */

get_header();

while ( have_posts() ) :
	the_post();

	get_template_part( 'template-parts/content', 'simple' );

endwhile; // End of the loop.

get_footer();
